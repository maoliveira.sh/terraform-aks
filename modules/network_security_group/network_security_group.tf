resource "azurerm_network_security_group" "public" {
    name                = "public"
    location            = var.resource_group.location
    resource_group_name = var.resource_group.name

    security_rule {
        name                       = "ssh"
        priority                   = 100
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}


resource "azurerm_subnet_network_security_group_association" "association1" {
    subnet_id = var.subnets.id 
    network_security_group_id   =   azurerm_network_security_group.public.id
  
}