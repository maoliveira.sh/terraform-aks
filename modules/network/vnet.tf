resource "azurerm_virtual_network" "aks" {
    name                =   "aks"
    resource_group_name =   var.resource_group.name
    location            =   var.resource_group.location
    address_space       =   ["10.1.0.0/16"]
}