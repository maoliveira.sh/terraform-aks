resource "azurerm_subnet" "eks_public" {
    name                    =   "eks_public"
    address_prefixes        =   ["10.1.1.0/24"]
    virtual_network_name    =   azurerm_virtual_network.aks.name
    resource_group_name     =   var.resource_group.name
}