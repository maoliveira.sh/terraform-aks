output "client_certificate" {
  value = module.aks.client_certificate
  
}

output "kube_config" {
  value = module.aks.kube_config
  
}